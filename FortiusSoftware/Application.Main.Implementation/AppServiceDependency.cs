﻿using Application.Main.Definition;
using DataAccess.Common.Definition;
using DataAccess.Common.Implementation;
using Microsoft.Practices.Unity;

namespace Application.Main.Implementation
{
    public static class AppServiceDependency
    {
        public static void InitializeAppService(this IUnityContainer container)
        {
            container.RegisterType<ISimulatorAppService, SimulatorAppService>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
        }
    }
}