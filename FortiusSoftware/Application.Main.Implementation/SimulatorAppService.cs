﻿using System;
using System.Collections.Generic;
using Application.Main.Definition;
using DataAccess.Common.Definition;
using Domain.Entities;

namespace Application.Main.Implementation
{
    public class SimulatorAppService : ISimulatorAppService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SimulatorAppService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddSimulator(Simulator simulator)
        {
            const string sql =
                "INSERT INTO dbo.Simulator (BorrowedCapital, Fee, Rate, CreditType, DateQuery, Name, LastName) " +
                "VALUES (@BorrowedCapital, @Fee, @Rate, @CreditType, @DateQuery, @Name, @LastName)";

            simulator.RateDecimal = Convert.ToDecimal(simulator.Rate);
            simulator.DateQuery = DateTime.UtcNow.ToLocalTime();

            _unitOfWork.Add(simulator, sql);
        }

        public List<Credit> GetCredit(Simulator s)
        {
            var creditLive = new List<Credit>();

            var k = (double) s.BorrowedCapital;
            var i = Convert.ToDouble(s.Rate)/100;
            var n = s.Fee;
            var fee = k*(i*Math.Pow(1 + i, n)/(Math.Pow(1 + i, n) - 1));

            for (var c = 1; c <= s.Fee; c++)
            {
                var rate = Math.Round(k*i);
                var amortization = fee - rate;

                var balance = k - amortization;

                var credit = new Credit
                {
                    Fee = fee,
                    Rate = rate,
                    Amortization = amortization,
                    Balance = balance,
                    NumberOfFee = c
                };
                k = balance;

                creditLive.Add(credit);
            }

            return creditLive;
        }

        public IEnumerable<CreditType> GetDataListValues()
        {
            const string sql = "SELECT Id, Name from [dbo].[CreditType]";
            var response = _unitOfWork.Get(sql);

            return response;
        }

        public IEnumerable<CreditFilter> GetStoreProcedure(DateTime dateInitial, DateTime dateEnd)
        {
            return _unitOfWork.GetStoreProcedure(dateInitial.ToShortDateString(), dateEnd.ToShortDateString());
        }

        public Simulator GetData(Guid id)
        {
             var sql =
                "select s.DateQuery, s.BorrowedCapital, s.Fee, s.Rate, s.Name, s.LastName, ct.Name as CreditType from [dbo].[Simulator] s inner join[dbo].[CreditType]ct on s.CreditType = ct.Id where s.Id = " + "'"+id+"'";
            var response = _unitOfWork.GetData(sql);

            return response;
        }
    }
}