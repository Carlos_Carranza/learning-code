﻿using System.Web.Optimization;

namespace FortiusSoftware
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region StyleBundle

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/xtensions").Include(
                "~/Content/lib/jqueryUI/jquery-ui.css"
            ));

            bundles.Add(new StyleBundle("~/Content/validations").Include(
                "~/Content/lib/jquery-validation-engine/validationengine.jquery.css"
            ));

            bundles.Add(new StyleBundle("~/Content/layout").Include(
                "~/Content/lib/bootstrap/bootstrap.css",
                "~/Content/lib/bootstrap/select2.css",
                "~/Content/lib/bootstrap/select2-bootstrap.css",
                "~/Content/lib/bootstrap/bootstrap-datetimepicker.min.css",
                "~/Content/lib/bootstrap/bootstrap-datepicker.css",
                "~/Content/lib/bootstrap/bootstrap-datepicker3.css",
                "~/Content/lib/kendo/kendo.css",
                "~/Content/lib/bootstrap/bootstrapValidator.min.css",
                "~/Content/portal/styles.css"
            ));

            bundles.Add(new StyleBundle("~/Content/kendo").Include(
                "~/Content/lib/kendo/2014.3.1119/kendo.common.min.css",
                "~/Content/lib/kendo/2014.3.1119/kendo.mobile.all.min.css",
                "~/Content/lib/kendo/2014.3.1119/kendo.dataviz.min.css",
                "~/Content/lib/kendo/2014.3.1119/kendo.material.min.css",
                "~/Content/lib/kendo/2014.3.1119/kendo.dataviz.material.min.css"
            ));

            bundles.Add(new StyleBundle("~/Content/xtensions").Include(
                "~/Content/lib/jqueryUI/jquery-ui.css"
            ));

            #endregion

            #region ScriptBundle

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/2014.3.1119/kendo.all.min.js",
                "~/Scripts/kendo/2014.3.1119/kendo.aspnetmvc.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                "~/Scripts/lib/jquery.livequery/jquery.livequery.js",
                "~/Scripts/lib/jquery.validationengine/jquery.validationengine.js",
                "~/Scripts/lib/jquery.validationengine/jquery.validationengine-es.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootstrapValidator.min.js",
                "~/Scripts/respond.js",
                "~/Scripts/select2.full.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/xtensions").Include(
                "~/Scripts/lib/JqueryUI/jquery-ui.min.js",
                "~/Scripts/lib/xtensions/x-general.js",
                "~/Scripts/lib/xtensions/x-inputs.js",
                "~/Scripts/lib/xtensions/x-modal.js",
                "~/Scripts/lib/xtensions/x-kendo.js",
                "~/Scripts/lib/xtensions/x-forms.js",
                "~/Scripts/lib/xtensions/x-navigation.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            #endregion

            bundles.IgnoreList.Clear();
            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);

            BundleTable.EnableOptimizations = false;
        }
    }
}