﻿using System.Linq;
using System.Web.Mvc;
using Application.Main.Definition;

namespace FortiusSoftware.Controllers
{
    public class DataController : Controller
    {
        private readonly ISimulatorAppService _simulatorAppService;

        public DataController(ISimulatorAppService simulatorAppService)
        {
            _simulatorAppService = simulatorAppService;
        }

        [AllowAnonymous]
        [OutputCache(Duration = int.MaxValue, VaryByParam = "self")]
        public ActionResult GetDataListValues(int? self, bool isLabel = false)
        {
            var results =  _simulatorAppService.GetDataListValues()
                    .Select(v => new
                    {
                        value = v.Id.ToString(),
                        text = v.Name
                    }).ToList();


            if (isLabel)
                results.Insert(0, new
                {
                    value = "0",
                    text = "Seleccione..."
                });
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}