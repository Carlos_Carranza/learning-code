﻿using System;
using System.Web.Mvc;
using Application.Main.Definition;
using Domain.Entities;

namespace FortiusSoftware.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISimulatorAppService _simulatorAppService;

        public HomeController(ISimulatorAppService simulatorAppService)
        {
            _simulatorAppService = simulatorAppService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Simulator(Simulator simulator)
        {
            _simulatorAppService.AddSimulator(simulator);
            return View("Results", _simulatorAppService.GetCredit(simulator));
        }

        public ActionResult Simulator()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FindByDate(DateTime dateInitial, DateTime dateEnd)
        {
            return View("Report", _simulatorAppService.GetStoreProcedure(dateInitial, dateEnd));
        }

        public ActionResult FindByDate()
        {
            return View();
        }

        public ActionResult ShowData(Guid id)
        {
            return View("ShowData", _simulatorAppService.GetData(id));
        }
    }
}