﻿using System;

namespace Domain.Entities
{
    public enum CreditTypeEnum
    {
        BuyCar = 1,
        BuyHouse = 2,
        FreeInvestment = 3,
        PurchaseOfPortfolio = 4,
        PaymentUniversity = 5,
        Entrepreneurship = 6,
    }

    public class CreditType
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}