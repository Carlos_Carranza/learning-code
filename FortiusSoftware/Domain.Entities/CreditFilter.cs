﻿using System;

namespace Domain.Entities
{
    public class CreditFilter
    {
        public long Number { get; set; }
        public string Name { get; set; }

        public DateTime Date { get; set; }

        public long Balance { get; set; }

        public string CreditType { get; set; }
        public Guid Id { get; set; }
    }
}