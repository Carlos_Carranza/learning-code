﻿namespace Domain.Entities
{
    public class Credit
    {
        public int NumberOfFee { get; set; }
        public double Fee { get; set; }

        public double Rate { get; set; }

        public double Amortization { get; set; }

        public double Balance { get; set; }
    }
}