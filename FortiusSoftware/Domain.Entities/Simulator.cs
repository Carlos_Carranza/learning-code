﻿using System;

namespace Domain.Entities
{
    public class Simulator
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public long BorrowedCapital { get; set; }
        
        public long Fee { get; set; }

        public string Rate { get; set; }

        public decimal RateDecimal { get; set; }

        public Guid CreditTypeId { get; set; }

        public string CreditType { get; set; }

        public DateTime DateQuery { get; set; }
    }
}