﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using DataAccess.Common.Definition;
using Domain.Entities;

namespace DataAccess.Common.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SqlConnection _sc =
            new SqlConnection("Data Source=RAZE\\LOCAL; Initial Catalog=Fortius; Integrated Security=true");

        private SqlDataAdapter _sda = new SqlDataAdapter();

        public void Add(Simulator simulator, string queryString)
        {
            var sqlCommand = new SqlCommand(queryString, _sc);

            sqlCommand.Parameters.Add("@BorrowedCapital", SqlDbType.BigInt).Value = simulator.BorrowedCapital;
            sqlCommand.Parameters.Add("@Fee", SqlDbType.BigInt).Value = simulator.Fee;
            sqlCommand.Parameters.Add("@Rate", SqlDbType.Decimal).Value = simulator.RateDecimal;
            sqlCommand.Parameters.Add("@CreditType", SqlDbType.UniqueIdentifier).Value = simulator.CreditTypeId;
            sqlCommand.Parameters.Add("@DateQuery", SqlDbType.DateTime).Value = simulator.DateQuery;
            sqlCommand.Parameters.Add("@Name", SqlDbType.VarChar).Value = simulator.Name;
            sqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar).Value = simulator.LastName;

            GetConection();
            sqlCommand.ExecuteNonQuery();
            DisposeConection();
        }

        public IEnumerable<CreditType> Get(string queryString)
        {
            var sqlCommand = new SqlCommand(queryString, _sc);

            var obj = new List<CreditType>();
            GetConection();
            var reader = sqlCommand.ExecuteReader();
            
            while (reader.Read())
                obj.Add(
                    new CreditType
                    {
                        Id = (Guid) reader["Id"],
                        Name = (string) reader["Name"]
                    }
                );
            DisposeConection();
            return obj;
        }

        public IEnumerable<CreditFilter> GetStoreProcedure(string dateInitial, string dateEnd)
        {
            var sqlCommand = new SqlCommand("[dbo].[SearchFilter]", _sc)
            {
                CommandType = CommandType.StoredProcedure
            };

            sqlCommand.Parameters.Add(new SqlParameter("@DateInitial", SqlDbType.DateTime)).Value = dateInitial;
            sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", SqlDbType.DateTime)).Value = dateEnd;

            var creditFilter = new List<CreditFilter>();
            GetConection();
            var reader = sqlCommand.ExecuteReader();

            if (reader.HasRows)
                while (reader.Read())
                    creditFilter.Add(
                        new CreditFilter
                        {
                            Number = (long) reader[0],
                            Name = (string) reader[1],
                            Date = (DateTime) reader[2],
                            Balance = (long) reader[3],
                            CreditType = (string) reader[4],
                            Id = (Guid)reader[5]
                        }
                    );
            reader.Close();
            DisposeConection();
            return creditFilter;
        }

        public Simulator GetData(string queryString)
        {
            var sqlCommand = new SqlCommand(queryString, _sc);

            GetConection();
            var reader = sqlCommand.ExecuteReader();

            while (reader.Read())
                return
                    new Simulator
                    {
                        DateQuery = (DateTime) reader["DateQuery"],
                        BorrowedCapital = (long) reader["BorrowedCapital"],
                        Fee = (long) reader["Fee"],
                        RateDecimal = Math.Round((decimal) reader["Rate"]),
                        Name = (string) reader["Name"],
                        LastName = (string) reader["LastName"],
                        CreditType = (string) reader["CreditType"],
                    };

            return null;

        }

        public void GetConection()
        {
            _sc.Open();
        }

        public void DisposeConection()
        {
            _sc.Close();
        }
    }
}