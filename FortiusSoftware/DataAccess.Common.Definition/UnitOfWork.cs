﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Domain.Entities;

namespace DataAccess.Common.Definition
{
    public interface IUnitOfWork
    {
        void Add(Simulator simulator, string queryString);
        IEnumerable<CreditType> Get( string queryString);
        IEnumerable<CreditFilter> GetStoreProcedure(string dateInitial, string dateEnd);
        Simulator GetData(string id);
    }
}