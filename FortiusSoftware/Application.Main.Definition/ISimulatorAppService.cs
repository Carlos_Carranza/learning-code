﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;

namespace Application.Main.Definition
{
    public interface ISimulatorAppService
    {
        void AddSimulator(Simulator simulator);
        List<Credit> GetCredit(Simulator s);
        IEnumerable<CreditType> GetDataListValues();

        IEnumerable<CreditFilter> GetStoreProcedure(DateTime dateInitial, DateTime dateEnd);

        Simulator GetData(Guid id);
    }
}