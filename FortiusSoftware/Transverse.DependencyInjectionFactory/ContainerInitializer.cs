﻿using Application.Main.Implementation;
using Microsoft.Practices.Unity;

namespace Transverse.DependencyInjectionFactory
{
    public static class ContainerInitializer
    {
        public static void InitializeContainer(this IUnityContainer container)
        {
            container.InitializeAppService();
        }
    }
}